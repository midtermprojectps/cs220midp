// ppm_io.c
// 601.220, Fall 2018
// Starter code for midterm project - feel free to edit/add to this file


#include "ppm_io.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {
  // printf("made it to read\n");
  // check that fp is not NULL
  assert(fp); 
  char c[3];
  fgets(c,3,fp);
  if (strcmp(c, "P6") != 0)
  {
    printf("Invallid PPM file");
    return NULL;//is this good to indicate to main program that there's an issue and we need to break out of everything?
  }
  int wid, len, colors;
  fscanf(fp, "%d %d %d", &wid, &len, &colors);//do we need '&' - yes we do
  //printf("wid: %d\nlen: %d\ncolors: %d\n", wid, len, colors);
  if (colors != 255)
  {
    printf("Error: PPM file contains color values outside the range 0-255");
    return NULL;
  }
  Image *i = malloc(sizeof(Image));
  
  Pixel *p = (Pixel*) malloc(sizeof(Pixel) * len * wid);
  fread(p, sizeof(Pixel), wid * len, fp);
  // Image *i = (Image*) malloc(3 * len * wid + 2 * sizeof(int));
  i->data = p;
  i->rows = len;
  i->cols = wid;

  return i;  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->rows * im->cols, fp);

  if (num_pixels_written != im->rows * im->cols) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

//overall seems to work find, just need to figure out the source of the parsing issue
