#include "imageManip.h"
#include "ppm_io.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

//swap
void swap(Image *img){
  for (int i = 0; i < img->rows * img->cols; i++){
    char temp = img->data[i].r;
    img->data[i].r = img->data[i].g;
    img->data[i].g = img->data[i].b;
    img->data[i].b = temp;
  }
}

//grayscale
void grayscale(Image *img){
  for (int i = 0; i < img->rows * img->cols; i++){
    float rf = 0.30 * (float) img->data[i].r;
    float gf = 0.59 * (float) img->data[i].g;
    float bf = 0.11 * (float) img->data[i].b;
    unsigned char intensity = (unsigned char)(rf + gf + bf);
    img->data[i].r = intensity;
    img->data[i].g = intensity;
    img->data[i].b = intensity;
  }
}

//contrast
unsigned char adjustVal(unsigned char c, double factor){
  double c2 = (double) c / 255.0 - 0.5;//convert
  c2  = 255.0 * ((c2 * factor) + 0.5);//convert back
  if (c2 > 255.0)
    c2 = 255.0;
  if (c2 < 0.0)
    c2 = 0.0;
  return (unsigned char) c2;
}

void contrast(Image *img, double factor){
  for (int i = 0; i < img->rows * img->cols; i++){
    img->data[i].r = adjustVal(img->data[i].r, factor);
    img->data[i].g = adjustVal(img->data[i].g, factor);
    img->data[i].b = adjustVal(img->data[i].b, factor);
  }
}

//zoom in
Image * zoom_in(Image *img){
  Image *newimg = (Image*) malloc(sizeof(Image));
  newimg->rows = img->rows * 2;
  newimg->cols = img->cols * 2;
  int rowlen = img->cols;
  newimg->data = (Pixel*) malloc(sizeof(Pixel) * newimg->rows * newimg->cols);
  for (int i = 0; i < img->rows * img->cols; i++){
    newimg->data[2 * i + rowlen * 2 * (i / rowlen)] = img->data[i];
    newimg->data[2 * i + rowlen * 2 * (i / rowlen) + 1] = img->data[i];
    newimg->data[2 * i + rowlen * 2 * (i / rowlen)] = img->data[i];
    newimg->data[2 * i + rowlen * 2 * (i / rowlen + 1) + 1] = img->data[i];
  }
  return newimg;
}

//zoom out
Pixel avgPixel(Pixel p1, Pixel p2, Pixel p3, Pixel p4){
  Pixel p;
  p.r = round(((double)(p1.r) + (double)(p2.r) + (double)(p3.r) + (double)(p4.r)) / 4.0);
  p.g = round(((double)(p1.g) + (double)(p2.g) + (double)(p3.g) + (double)(p4.g
)) / 4.0);
  p.b = round(((double)(p1.b) + (double)(p2.b) + (double)(p3.b) + (double)(p4.b
)) / 4.0);
  return p;
}

Image * zoom_out(Image *img){
  int rL = img->cols;//row length
  int newRL = rL / 2;
  Image *newimg = (Image*) malloc(sizeof(Image));
  newimg->rows = img->rows / 2;
  newimg->cols = newRL;
  newimg->data = (Pixel*) malloc(sizeof(Pixel) * newimg->rows * newimg->cols);
  for (int i = 0; i < newimg->rows * newRL; i++){
    //find 4 pixels to average
    Pixel p1 = img->data[rL * (i / newRL) * 2 + (i % rL) * 2];
    Pixel p2 = img->data[rL * (i / newRL) * 2 + (i % rL) * 2 + 1];
    Pixel p3 = img->data[rL * ((i / newRL) * 2 + 1) + (i % rL) * 2];
    Pixel p4 = img->data[rL * ((i / newRL) * 2 + 1) + (i % rL) * 2 + 1];
    //now average them
    newimg->data[i] = avgPixel(p1, p2, p3, p4);
  }
  return newimg;
}

//occlude
void occlude(Image *img, int x1, int y1, int x2, int y2){
  for (int i = y1; i <= y2; i++){
    for (int j = x1; j <= x2; j++){
      img->data[i * img->cols + j].r = 0;
      img->data[i * img->cols + j].g = 0;
      img->data[i * img->cols + j].b = 0;
    }
  }
}

//blur
double gauss(int dX, int dY, double sigma){
  return (1.0 / (2.0 * M_PI * pow(sigma, 2))) * exp( -(pow(dX, 2) + pow(dY, 2)) / (2 * pow(sigma, 2)));
}

double * getKernel(double sigma, int ksize){
  double *kernel = malloc(sizeof(double) * ksize);
  int center = (ksize * ksize) / 2;//center index
  int centerY = center / ksize;//"convert" to 2d
  int centerX = center % ksize;
  for (int i = 0; i < ksize; i++){
    //get dX and dY by comparing to center in 2d
    int iY = i / ksize;
    int iX = i % ksize;
    int dY = iY - centerX;
    int dX = iX - centerY;
    //now call gaussian function
    kernel[i] = gauss(dX, dY, sigma);
  }
  return kernel;
}



//blur




  

int max(int a, int b){
  if (a >= b)//we don't care if they're equal
    return a;
  return b;
}

int min(int a, int b){
  if (a <= b)
    return a;
  return b;
}

Pixel convolve(Image *img, double *kernel, int ksize, int i){
  int iY = i / img->cols;//2d
  int iX = i % img->cols;
  int k = 0;
  double sumR, sumG, sumB, divisor = 0.0;
  //iterate through part of image that we are convolving
  for (int y = max(0, iY - ksize / 2); y < min(iY + ksize / 2, img->rows); y++)
  {
    for (int x = max(0, iX - ksize / 2); x < min(iX + ksize / 2, img->cols); x++)
    {
      int j = y * img->cols + x;//convert back to 1d
      sumR += img->data[j].r * kernel[k];
      sumG += img->data[j].g * kernel[k];
      sumB += img->data[j].b * kernel[k];
      divisor += kernel[k];
      k++;
    }
  }
  Pixel p;
  p.r = round(sumR / divisor);
  p.g = round(sumG / divisor);
  p.b = round(sumB / divisor);
  return p;
}

Image * blur(Image *img, double sigma){
  Image *newimg = (Image*) malloc(sizeof(Image));
  newimg->rows = img->rows;
  newimg->cols = img->cols;
  newimg->data = (Pixel*) malloc(sizeof(Pixel) * img->rows * img->cols);
  int ksize = round(sigma * 10);//have this here b/c we need ksize for convolve
  ksize += ksize % 2;
  double *kernel = getKernel(sigma, ksize);
  for (int i = 0; i < img->rows * img->cols; i++)
    newimg->data[i] = convolve(img, kernel, ksize, i);
  return newimg;
}
