#include "ppm_io.h"
#include "imageManip.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
  //read file and function names
  if (argc < 3)//either of file names is not inputted
    return 1;
  FILE *fp;
  if (!(fp = fopen(argv[1], "rb")))//couldn't open file
    return 2;
  Image *img;
  if (!(img = read_ppm(fp)))//reading file into image fails
    return 3;
  if (argc < 4)//no function names
    return 4;
    
  //call functions
  /*basically we first check for incorrect # of params or types of params and if everything's ok , we call the function*/
  char *function = argv[3];//function name
  //swap
  if (!strcmp(function, "swap")){
    if (argc != 4)//expected length is 4 since no extra params
      return 5;
    swap(img);
  }

  //grayscale
  else if (!strcmp(function, "grayscale")){
    if (argc != 4)
      return 5;
    grayscale(img);
  }
      
  //contrast
  else if (!strcmp(function, "contrast")){
    if (argc != 5)
	return 5;
    double factor;//to be set to double converted from extra param
    if (!(factor = atof(argv[4])))//has to be a double
	return 5;
    contrast(img, factor);
  }
    
  //zoom_in
  else if (!strcmp(function, "zoom_in")){
      if (argc != 4)
	return 5;
      img = zoom_in(img);
    }

  //zoom_out
  else if (!strcmp(function, "zoom_out")){
      if (argc != 4)
	return 5;
      img = zoom_out(img);
    }

  //occlude
  else if (!strcmp(function, "occlude")){
    if (argc != 8)
      return 5;
    int x1, y1, x2, y2;
    if (!(x1 = atoi(argv[4])))//all have to be ints
      return 5;
    if (!(y1 = atoi(argv[5])))
      return 5;
    if (!(x2 = atoi(argv[6])))
      return 5;
    if (!(y2 = atoi(argv[7])))
      return 5;
    if (x1 < 0 || x2 > img->cols || y1 < 0 || y2 > img->rows)
      return 6;//out of bounds
    occlude(img, x1, y1, x2, y2);
  }

  //blur
  else if (!strcmp(function, "blur")){
    if (argc != 5)
      return 5;
    double sigma;
    if (!(sigma = atof(argv[4])))
      return 5;
    img = blur(img, sigma);
  }

  //invalid function name
  else{
    return 4;
  }

  //now write to output file
  FILE *fout;
  if (!(fout = fopen(argv[2], "wb")))
    return 7;
  if (write_ppm(fout, img) < img->rows * img->cols)
    return 7;
  
}
