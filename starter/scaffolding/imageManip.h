#ifndef IMAGE_MANIP_H
#define IMAGE_MANIP_H

#include <stdio.h>
#include "ppm_io.h"

void swap(Image *img);
void grayscale(Image *img);
void contrast(Image *img, double factor);
Image * zoom_in(Image *img);
Image * zoom_out(Image *img);
Pixel avgPixel(Pixel p1, Pixel p2, Pixel p3, Pixel p4);
void occlude(Image *img, int x1, int x2, int y1, int y2);
Image * blur(Image *img, double sigma);
double * getKernel(double sigma, int ksize);
double gauss(int dX, int dY, double sigma);
Pixel convolve(Image *img, double *kernel, int ksize, int i);
int max(int a, int b);
int min(int a, int b);

#endif
