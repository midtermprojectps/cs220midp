#include "ppm_io.h"
#include "imageManip.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
  //read file and function names
  char *filename, *filename2, *function;
  if (scanf(" %s ", filename1) == 0)
    return 1;
  if (scanf(" %s ", filename2) == 0)
    return 1;
  if (!(FILE *fp = fopen(filename1, 'r')))
    return 2;
  if (!(Image *img = read_ppm(fp)))
    return 3;
  if (scanf(" %s ", function) == 0)
    return 4;
  //call functions
  switch (function){
  case "swap":
    double extra;
    if (scanf(" %f ", &extra) > 0)//should be no more arguments
      return 5;
    swap(img);
    break;
  case "grayscale":
    double extra;
    if (scanf(" %f ", &extra) > 0)//should be no more arguments
      return 5;
    grayscale(img);
    break;
  case "contrast":
    double factor, extra;
    if (scanf(" %f ", &factor) != 1)
      return 5;
    if (scanf(" %f ", &extra) > 0)//should be no more arguments
      return 5;
    contrast(img, factor);
  case "zoom_in":
    double extra;
    if (scanf(" %f ", &extra) > 0)//should be no more arguments
      return 5;
    img = zoom_in(img);
  case "zoom_out":
    double extra;
    if (scanf(" %f ", &extra) > 0)//should be no more arguments
      return 5;
    img = zoom_out(img);
  case "occlude":
    int x1, y1, x2, y2;
    if (scanf(" %d %d %d %d ", x1, y1, x2, y2) != 4)
      return 5;
    double extra;
    if (scanf(" %f ", &extra) > 0)//should be no more arguments
      return 5;
    if (x1 < 0 || x2 > img->cols || y1 < 0 || y2 > img->rows;
    occlude(img, x1, y1, x2, y2);
  case "blur":
    double sigma;
    if (scanf(" %d ", &sigma) != 1)
      return 5;
